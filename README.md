  # C4 diagram processing with Mermaid
Foster discussion about a meaningful subsystem by isolating parts of complex [C4 diagrams](https://c4model.com/).

While C4 context diagrams help in understanding everything about the interactions of a system with other systems and users, producing and analyzing them may be complex due to their amplitude.

# Example
Suppose a citizen needs to use a system (System1), that uses another system (System2), which uses yet another system (System3). To model the citizen's experience in a sound and complete way, it helps in focusing just on the system they have direct visibility over. 

It will be easier to interview them and verify the diagram is correct.
## Before

```mermaid
    C4Context  
      title System Context diagram for ido  
      Enterprise_Boundary(b0, "ido boundary") {  
        Person(p1, "citizen A", "A citizen using the mobile app.")  
        System(s1,"System1")  
        System(s2,"System2")  
        System(s3,"System3")  
        Rel(p1,s1,"uses")  
        Rel(s1,s2,"uses")  
        Rel(s2,s3,"uses")
        }
```

## After
```mermaid
    C4Context  
      title System Context diagram for ido  
      Enterprise_Boundary(b0, "ido boundary") {  
        Person(p1, "citizen A", "A citizen using the mobile app.")  
        System(s1,"System1")  
        Rel(p1,s1,"uses")  
        }
```
# How to use it
```python
from mermaid_processing.mermaid_parser import MermaidParser
mp=MermaidParser(None)  
with open("ido.md") as f:  
    content = f.read()  
    mp.parse(content)
    lines=mp.get_lines_for_diagram_with_point_of_view_of("IdoDataStore")
    diagram=mp.render(lines)  
    print(diagram)
```