import unittest
import os

from mermaid_processing.mermaid_parser import MermaidParser


class MyTestCase(unittest.TestCase):
    def test_point_of_view(self):
        mp = self.get_mermaid_parser()
        identfiers = mp.identifiers
        expected_identifiers = ["s1", "s2", "s3", "p1"]
        for identifier in expected_identifiers:
            self.assertIn(identifier, identfiers)
        self.assertTrue(mp.exists_relationship("s1", "s2"))
        self.assertFalse(mp.exists_relationship("s2", "s3"))
        self.assertTrue(mp.exists_relationship("p1", "s1"))

        lines_with_point_of_view = mp.get_lines_for_diagram_with_point_of_view_of("s1")
        expected_substring = '        System(s2,"System2")'
        self.assertIn(expected_substring, lines_with_point_of_view)

    def get_mermaid_parser(self):
        with open("data/c4_with_three_systems_and_one_user.md") as f:
            content = f.read()
        mp = MermaidParser(None)
        mp.parse(content)
        return mp

    def test_has_exactly_one_identifier(self):
        mp = self.get_mermaid_parser()
        self.assertTrue(mp.contains_exactly_one_identifier("System(s1)"))
        self.assertFalse(mp.contains_exactly_one_identifier("System(s1,s2)"))
        self.assertTrue(mp.contains_exactly_one_identifier('System(s1,"s2","s3")'))


if __name__ == "__main__":
    unittest.main()
