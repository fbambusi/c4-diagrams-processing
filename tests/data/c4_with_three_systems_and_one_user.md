C4Context
      title System Context diagram for ido
      Enterprise_Boundary(b0, "ido boundary") {
        Person(p1, "citizen A", "A citizen using the mobile app.")
        System(s1,"System1")
        System(s2,"System2")
        System(s3,"System3")
        Rel(p1,s1,"uses")
        Rel(s1,s2,"uses")
        Rel(s2,s3,"uses")
        }
