from typing import List


class NoIdentifierException(Exception):
    pass


class MermaidParser(object):
    identifiers = []
    lines: List[str] = []

    def __init__(self, config):
        self.config = config

    def parse(self, data: str):
        # Parse the data and return a list of Mermaid objects
        self.lines = data.splitlines()
        self.identifiers = []
        for line in self.lines:
            if self.contains_identifier(line):
                self.identifiers.append(self.get_identifier(line))
        self.identifiers = list(set(self.identifiers))

    def contains_identifier(self, line):
        # lines with identifiers: KEYWORD(identifier) or KEYWORD(identifier,identifier) or KEYWORD(identifier,identifier,identifier)
        keywords_before_parentheses = ["System", "Person", "System_Ext", "Rel"]
        if any(keyword in line for keyword in keywords_before_parentheses):
            line = line.strip()
            line_part_within_brackets = line[line.find("(") + 1 : line.find(")")]
            if len(line_part_within_brackets) > 0:
                return True

    def contains_exactly_one_identifier(self, line):
        if self.contains_identifier(line):
            number_of_commas_between_parentheses = line[
                line.find("(") + 1 : line.find(")")
            ].count(",")
            if number_of_commas_between_parentheses == 0:
                return True
            part_of_line_withting_brackets = line[line.find("(") + 1 : line.find(")")]
            tokens = part_of_line_withting_brackets.split(",")
            tokens_without_quotes = [t for t in tokens if t.find('"') < 0]
            return len(tokens_without_quotes) == 1
        else:
            return False
        return False

    def get_identifier(self, line_with_identifier, index=0):
        if not self.contains_identifier(line_with_identifier):
            raise NoIdentifierException(
                "No identifier found in line: " + line_with_identifier
            )
        part_of_line_withting_brackets = line_with_identifier[
            line_with_identifier.find("(") + 1 : line_with_identifier.find(")")
        ]
        if part_of_line_withting_brackets.find(",") > 0:
            return part_of_line_withting_brackets.split(",")[index].strip()

    def exists_relationship(self, identifier_1, identifier_2):
        for line in self.lines:
            try:
                if (
                    self.get_identifier(line, 0) == identifier_1
                    and self.get_identifier(line, 1) == identifier_2
                ):
                    return True
                if (
                    self.get_identifier(line, 1) == identifier_1
                    and self.get_identifier(line, 0) == identifier_2
                ):
                    return True
            except NoIdentifierException:
                pass
        return False

    def get_lines_for_diagram_with_point_of_view_of(self, param):
        interested_identifiers = [param]
        iss = [i for i in self.identifiers if self.exists_relationship(i, param)]

        lines = []
        for line in self.lines:
            if self.contains_identifier(line):
                if (
                    self.get_identifier(line, 0) == param
                    or self.get_identifier(line, 1) == param
                ):
                    lines.append(line)
                elif self.contains_exactly_one_identifier(line):
                    if (
                        self.get_identifier(line, 0) in iss
                        or self.get_identifier(line, 1) in iss
                    ):
                        lines.append(line)
            else:
                lines.append(line)

        return lines

    def render(self, lines):
        return "\n".join([l for l in lines if l.strip() != ""])
